<?php
require_once('../phplib/header.php');

?>
<head>
    <meta name="description" content=" Four Point Probe Station">
    <meta name="keywords" content="Jandel Four Point Probe System,Agilent E4443A spectrum analyser, RF impedance  analysers, high-frequency Capacitance-Voltage analysers, digital oscilloscopes">
    <meta name="author" content=" Nanofabrication Centre Southampton">
</head>
<body>
<div class="container">
	<?php require_once('../phplib/graphicsTop.php');?>
	<div class="span-24">
	<?php require_once('../phplib/menuTop.php');?>
	</div>
	<div class="span-24 last">&nbsp;</div>
	<div class="span-24 last">
	<div class="span-5" id="nav"><?php include 'menuCharacterisation.php';?></div>
	<div class="span-13">
		<!-- content goes here-->
		<p>A  Jandel Four Point Probe System is available for resistivity measurements on  wafers up to 8 inch (similar to v/d Pauw measurements). A large selection of  analysers is available to connect to any of the above system or to stand alone devices. This includes an Agilent E4443A spectrum analyser, RF impedance  analysers, high-frequency Capacitance-Voltage analysers, digital oscilloscopes, and signal generators.</p>
		<!--end of content-->
	</div>
		<div class='span-6  sideBar last'>
	<!-- side bar content-->

	<!-- end of sidebar-->
	</div>
	</div>
	<div class="span-16">&nbsp;</div>
	<div class="span-1">&nbsp;</div>
	<div class="span-7 last">&nbsp;</div>
	<div class="span-24 last footer"><?php require_once('../phplib/footer.php');?></div>
	<div class="span-24">&nbsp;</div>
</div>

</body>
</html>
